package ru.nspk.bank;

import org.junit.Before;
import org.junit.Test;
import ru.nspk.bank.bank.Account;
import ru.nspk.bank.bank.Bank;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BankTest {

    protected static final int START_AMOUNT_OF_MONEY = 100;
    private Bank bank;

    private Account accountA;
    private String accountNumberA;

    private Account accountB;
    private String accountNumberB;


    @Before
    public final void setUp() throws Exception {
        accountNumberA = "A";
        accountNumberB = "B";
        accountA = new Account(accountNumberA, START_AMOUNT_OF_MONEY);
        accountB = new Account(accountNumberB, START_AMOUNT_OF_MONEY);
        bank = new Bank(Arrays.asList(accountA, accountB), Collections.emptyList());
    }

    @Test
    public void whenVerificationFailedThenEachAccountBlocks() throws Exception {
        bank.onVerificationFailed(accountNumberA, accountNumberB);
        assertTrue(accountA.isBlocked());
        assertTrue(accountB.isBlocked());
    }

    @Test
    public void whenOnlyOneAccountFailedVerificationThenOtherNotBlocked() throws Exception {
        bank.onVerificationFailed(accountNumberA);
        assertTrue(accountA.isBlocked());
        assertFalse(accountB.isBlocked());
    }

    @Test
    public void transferMoneyCorrectly() throws Exception {
        bank.transfer(accountNumberA, accountNumberB, START_AMOUNT_OF_MONEY);
        assertEquals(0, bank.getBalance(accountNumberA));
        assertEquals(2 * START_AMOUNT_OF_MONEY, bank.getBalance(accountNumberB));
    }
}

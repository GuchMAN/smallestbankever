package ru.nspk.bank;

import org.junit.Before;
import org.junit.Test;
import ru.nspk.bank.bank.Account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AccountTest {
    private static final int INITIAL_ACCOUNT_BALANCE = 100;
    private Account account;

    @Before
    public final void setUpAccount() throws Exception {
        account = new Account("number", INITIAL_ACCOUNT_BALANCE);
    }

    @Test
    public void whenBalanceLowThenDecreaseReturnFalse() throws Exception {
        assertFalse(account.decrease(INITIAL_ACCOUNT_BALANCE + 1));
    }

    @Test
    public void whenEnoughBalanceThenDecreaseReturnsTrue() throws Exception {
        assertTrue(account.decrease(INITIAL_ACCOUNT_BALANCE));
    }

    @Test
    public void balanceDecreaseProperly() throws Exception {
        final int expectedRemains = 20;
        account.decrease(INITIAL_ACCOUNT_BALANCE - expectedRemains);
        assertEquals(expectedRemains, account.getBalance());
    }

    @Test
    public void balanceIncreasedProperly() throws Exception {
        final int addedAmount = 10;
        long balanceAfterAppend = account.append(addedAmount);
        assertEquals(INITIAL_ACCOUNT_BALANCE + addedAmount, balanceAfterAppend);
    }
}

package ru.nspk.bank.bank;

import static java.lang.String.format;

// TODO separate business exceptions by types (out of scope)
public class TransferTransactionException extends RuntimeException {
    public TransferTransactionException(Account from, Account to, long amount) {
        super(
                format(
                        "Couldn't transfer money from %s to %s accounts. Amount: %s",
                        from.getNumber(),
                        to.getNumber(),
                        amount
                )
        );
    }
}

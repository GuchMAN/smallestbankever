package ru.nspk.bank.bank;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class Account {

    private final AtomicLong money;

    private final String accNumber;

    private final AtomicBoolean isBlocked;

    public Account(String accNumber, long money) {
        this.accNumber = accNumber;
        this.money = new AtomicLong(money);
        this.isBlocked = new AtomicBoolean(false);
    }

    public String getNumber() {
        return accNumber;
    }

    public long getBalance() {
        return money.get();
    }

    public long append(long amount) {
        return money.addAndGet(amount);
    }

    public boolean decrease(long amount) {
        if (money.get() < amount) {
            return false;
        }
        money.getAndAdd(-amount);
        return true;
    }

    public void block() {
        isBlocked.set(true);
    }

    public boolean isBlocked() {
        return isBlocked.get();
    }
}
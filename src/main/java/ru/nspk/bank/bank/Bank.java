package ru.nspk.bank.bank;

import ru.nspk.bank.listeners.BankOperationListener;
import ru.nspk.bank.listeners.VerificationListener;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
public class Bank implements VerificationListener {

    private final Map<String, Account> accounts;
    private List<BankOperationListener> bankOperationListeners;

    public Bank(List<Account> accounts, List<BankOperationListener> bankOperationListeners) {
        // ConcurrentHashMap in case that Bank could create account in runtime
        this.accounts = accounts.stream().collect(
                Collectors.toConcurrentMap(
                        Account::getNumber,
                        account -> account
                )
        );
        this.bankOperationListeners = bankOperationListeners != null ? bankOperationListeners : Collections.emptyList();
    }

    public void transfer(String fromAccountNum, String toAccountNum, long amount) throws TransferTransactionException {
        final Account from = accounts.get(fromAccountNum);
        final Account to = accounts.get(toAccountNum);

        if (!transfer(from, to, amount)){
            throw new TransferTransactionException(from, to, amount);
        }

        bankOperationListeners.forEach(l -> l.onTransfer(fromAccountNum, toAccountNum, amount));
    }

    private boolean transfer(Account from, Account to, long amount) {
        final boolean reverseLockOrder = to.getNumber().compareTo(from.getNumber()) < 0; // assume from != to by validation

        // reverse order for avoid of A-B B-A accounts transaction deadlock
        final Account firstToLock = reverseLockOrder ? to : from;
        final Account secondToLock = reverseLockOrder ? from : to;
        synchronized (firstToLock) {
            if (firstToLock.isBlocked()) { // small optimization
                return false;
            }
            synchronized (secondToLock) {
                if (secondToLock.isBlocked() || !from.decrease(amount)) {
                    return false;
                }
                to.append(amount);
            }
        }
        return true;
    }

    public long getBalance(String accountNum) {
        // assume that existence of account with accountNum guaranteed by validation on previous layer.
        return accounts.get(accountNum).getBalance();
    }

    @Override
    public void onVerificationFailed(String... accountsNum) {
        for (String accountNum : accountsNum) {
            final Account account = accounts.get(accountNum);
            if (!account.isBlocked()) {
                synchronized (account) { // small optimization
                    account.block();
                }
            }
        }
    }
}
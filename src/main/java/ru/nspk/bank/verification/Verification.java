package ru.nspk.bank.verification;

import ru.nspk.bank.listeners.BankOperationListener;
import ru.nspk.bank.listeners.VerificationListener;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

public class Verification implements BankOperationListener {

    private final ExecutorService verificationExecutor;
    private final List<VerificationListener> verificationListeners;

    protected static final int MAX_AUTO_ACCEPT_AMOUNT = 50_000; // TODO create business requirements classes

    public Verification(List<VerificationListener> verificationListeners) {
        this.verificationExecutor = Executors.newSingleThreadExecutor();
        this.verificationListeners = verificationListeners != null ? verificationListeners : Collections.emptyList();
    }

    private boolean isFraud(
            String fromAccountNum,
            String toAccountNum,
            long amount
    ) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ignore) {
        }
        return ThreadLocalRandom.current().nextBoolean();
    }

    @Override
    public void onTransfer(String fromAccountNum, String toAccountNum, long amount) {
        if (amount <= MAX_AUTO_ACCEPT_AMOUNT) {
            return;
        }

        verificationExecutor.submit(() -> {
            if (isFraud(fromAccountNum, toAccountNum, amount)) {
                verificationListeners.forEach(l -> l.onVerificationFailed(toAccountNum, fromAccountNum));
            }
        });
    }
}

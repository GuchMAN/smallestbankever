package ru.nspk.bank.listeners;

public interface BankOperationListener {
    default void onTransfer(
            String fromAccountNum,
            String toAccountNum,
            long amount
    ){}
}

package ru.nspk.bank.listeners;

public interface VerificationListener {
    default void onVerificationFailed(String...accountsNum){}
}
